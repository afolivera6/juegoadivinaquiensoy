console.log ("*** AdivinaQuinSoy ***");

let personajes = [{
    name:['Mario Bros','Mario','MarioBros'],
    foto:'mario.png',
    preguntas:['Es Animal?','Es de Nintendo?','Tiene Bigote?','Usa Armas?','Es Viejo?'],
    respuetas:['no','si','si','no','si'],
}, {
    name:['Goku','Son Goku','Kakaroto'],
    foto:'goku.png',
    preguntas:['Vuela?','Usa Armadura?','Tiene Poderes ?','Usa Armas?','Es Ser Humano?'],
    respuetas:['si','no','si','no','no'],
},
    {
    name:['IronMan','Iron Man','Tony','Tony Stark','Stark'],
    foto:'ironman.png',
    preguntas:['Es Gigante?','Usa Armadura?','Es Super Soldado?','Estuvo en el espacio?','Ha Muerto?'],
    respuetas:['no','si','no','si','si'],
},
    {
    name:['Sonico','Sonic','El Erizo Azul'],
    foto:'sonic.png',
    preguntas:['Es Veloz?','Es Rojo?','Tiene Poderes?','Usa Armas?','Es Un Animal?'],
    respuetas:['si','no','si','no','si'],
},
    {
    name:['ryu'],
    foto:'ryu.png',
    preguntas:['Es Humano?','Sabe Karate?','Tiene Poderes?','Pertenece a un juego de peleas?','Es malvado?'],
    respuetas:['si','si','si','si','no'],
}
];


const btnJugar = document.getElementById("btnJugar");
const imgPersonaje = document.getElementById("imgPersonaje");
let indice = parseInt(Math.random() * 5);
let opacidad = 20;
let puntaje = 0;


btnJugar.addEventListener('click', () =>{

    const pregunta0 = document.getElementById("pregunta0");
    const pregunta1 = document.getElementById("pregunta1");
    const pregunta2 = document.getElementById("pregunta2");
    const pregunta3 = document.getElementById("pregunta3");
    const pregunta4 = document.getElementById("pregunta4");



    /* pregunta1 a la pregunta4 */

    imgPersonaje.src = "./assets/img/" + personajes[indice].foto;
    imgPersonaje.style.filter = "blur(20px)";

    pregunta0.value = personajes[indice].preguntas[0];
    pregunta1.value = personajes[indice].preguntas[1];
    pregunta2.value = personajes[indice].preguntas[2];
    pregunta3.value = personajes[indice].preguntas[3];
    pregunta4.value = personajes[indice].preguntas[4];
     /* cargar el resto de pregutas */ 


})


// progar las respuestas recividas en el select

const rta0 = document.getElementById("rta0");

rta0.addEventListener('change',()=> {

        if(rta0.value == personajes[indice].respuetas[0]) {
            opacidad = opacidad - 4;
            puntaje = puntaje + 1;
            document.getElementById("puntajee").innerHTML = "<center><h1>PUNTAJE</H1><H2>"+ puntaje +"</H2></center>";

            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta0").src = "./assets/img/si.png";

        } else {
            document.getElementById("icoRta0").src = "./assets/img/no.png";
        }

        rta0.disabled = true;
})

const rta1 = document.getElementById("rta1");

rta1.addEventListener('change',()=> {

        if(rta1.value == personajes[indice].respuetas[1]) {
            opacidad = opacidad - 4;
            puntaje = puntaje + 1;
            document.getElementById("puntajee").innerHTML = "<center><h1>PUNTAJE</H1><H2>"+ puntaje +"</H2></center>";

            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta1").src = "./assets/img/si.png";

        } else {
            document.getElementById("icoRta1").src = "./assets/img/no.png";
        }

        rta1.disabled = true;
})


const rta2 = document.getElementById("rta2");

rta2.addEventListener('change',()=> {

        if(rta2.value == personajes[indice].respuetas[2]) {
            opacidad = opacidad - 4;
            puntaje = puntaje + 1;
            document.getElementById("puntajee").innerHTML = "<center><h1>PUNTAJE</H1><H2>"+ puntaje +"</H2></center>";

            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta2").src = "./assets/img/si.png";

        } else {
            document.getElementById("icoRta2").src = "./assets/img/no.png";
        }

        rta2.disabled = true;
})

const rta3 = document.getElementById("rta3");

rta3.addEventListener('change',()=> {

        if(rta3.value == personajes[indice].respuetas[3]) {
            opacidad = opacidad - 4;
            puntaje = puntaje + 1;
            document.getElementById("puntajee").innerHTML = "<center><h1>PUNTAJE</H1><H2>"+ puntaje +"</H2></center>";

            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta3").src = "./assets/img/si.png";

        } else {
            document.getElementById("icoRta3").src = "./assets/img/no.png";
        }

        rta3.disabled = true;
})

const rta4 = document.getElementById("rta4");

rta4.addEventListener('change',()=> {

        if(rta4.value == personajes[indice].respuetas[4]) {
            opacidad = opacidad - 4;
            puntaje = puntaje + 1;
            document.getElementById("puntajee").innerHTML = "<center><h1>PUNTAJE</H1><H2>"+ puntaje +"</H2></center>";

            imgPersonaje.style.filter = "blur(" + opacidad +  "px)";
            document.getElementById("icoRta4").src = "./assets/img/si.png";

        } else {
            document.getElementById("icoRta4").src = "./assets/img/no.png";
        }

        rta4.disabled = true;
})




//RESULTADOS

const btnRespuesta = document.getElementById("btnRespuesta");

btnRespuesta.addEventListener('click' , () => {

    const RespuestaGeneral = document.getElementById("RespuestaGeneral").value

    respuestaUsu = RespuestaGeneral.toLowerCase();
    console.log ("Respuesta General " + respuestaUsu)
    validarRespuestas(respuestaUsu, indice);

})


//validaciones de respuesta

const validarRespuestas = (nombre, i) =>
{
    const imgResultado = document.getElementById("imgResultado");
    let bandera = false;
    personajes[i].name.forEach(nomPer =>
    {
        if (nombre == nomPer) 
        {
            bandera = true;
        }
    })
    if (bandera == true) 
    {
        imgResultado.src ="./assets/img/trofeo.png";
        console.log("Ganador");
    }else{
        imgResultado.src ="./assets/img/gameOver.png";
        console.log("Perdio");
    }
}